package com.example.chimkontran.notetakerapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

// s3480554
// Tran Truong Vu

public class MainActivity extends AppCompatActivity {

    // Variables
    private ListView nListViewNotes;

    // Log activity life cycle onCreate, onResume, onPause, onRestart, onStop methods

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("Status", "App is running onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Get Listview
        nListViewNotes = (ListView) findViewById(R.id.main_listview_notes);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i("Status: ", "App is running onCreate");
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    // Selected button action
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_main_note_new:
                // Start new NoteActivity
                Intent newNoteActivity = new Intent(this, NoteActivity.class);
                startActivity(newNoteActivity);
                break;
        }
        return true;
    }

    @Override
    protected void onResume() {
        Log.i("Status", "App is running onResume");
        super.onResume();
        nListViewNotes.setAdapter(null);

        // Display NOTEs
        ArrayList<Note> notes = Utilities.getAllSavedNotes(this);

        // If no note
        if (notes == null || notes.size() == 0) {
            Toast.makeText(this, "You have no saved note", Toast.LENGTH_SHORT).show();
            return;
        } else {
            NoteAdapter noteAdapter = new NoteAdapter(this, R.layout.item_note, notes);
            nListViewNotes.setAdapter(noteAdapter);

            // When Click on NOTE
            nListViewNotes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    String fileName = ((Note) nListViewNotes.getItemAtPosition(position)).getnDateTime()
                            + Utilities.File_Extension;

                    // Move to NoteActivity
                    Intent viewNoteIntent = new Intent(getApplicationContext(), NoteActivity.class);
                    viewNoteIntent.putExtra("NOTE_FILE", fileName);
                    startActivity(viewNoteIntent);
                }
            });
        }
    }

    @Override
    protected void onRestart() {
        Log.i("Status", "App is running onRestart");
        super.onRestart();
    }

    @Override
    protected void onPause() {
        Log.i("Status", "App is running onPause");
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.i("Status", "App is running onStop");
        super.onStop();
    }
}
