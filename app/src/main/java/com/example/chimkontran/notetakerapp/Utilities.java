package com.example.chimkontran.notetakerapp;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by chimkontran on 11/1/2017.
 */

public class Utilities {

    // Variables
    public static final String File_Extension = ".bin";


    public static boolean saveNote(Context context, Note  note)
    {
        String fileName = String.valueOf(note.getnDateTime()) + File_Extension;

        FileOutputStream fileOutputStream;
        ObjectOutputStream objectOutputStream;

        try
        {
            // Open FILE
            fileOutputStream = context.openFileOutput(fileName, context.MODE_PRIVATE);

            // Write on FILE
            objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(note);

            // Close FILE
            objectOutputStream.close();
            fileOutputStream.close();

        } catch (Exception e)
        {
            e.printStackTrace();
            return false; // Tell user something went wrong
        }

        return true;
    }


    public static ArrayList<Note> getAllSavedNotes(Context context) {
        ArrayList<Note> notes = new ArrayList<>();

        // Get FILE Direction
        File filesDir = context.getFilesDir();
        ArrayList<String> noteFiles = new ArrayList<>();

        // List of file
        for (String file : filesDir.list()) {
            if (file.endsWith(File_Extension)) {
                noteFiles.add(file);
            }
        }

        FileInputStream fileInputStream;
        ObjectInputStream objectInputStream;

        for (int i = 0; i < noteFiles.size(); i++) {
            try {
                // Write FILE
                fileInputStream = context.openFileInput(noteFiles.get(i));
                objectInputStream = new ObjectInputStream(fileInputStream);

                // Read FILE
                notes.add((Note) objectInputStream.readObject());

                // Close FILE
                fileInputStream.close();
                objectInputStream.close();

            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
                return null;
            }
        }
        return notes;
    }

    public static Note getNoteByName(Context context, String fileName) {
        File file = new File(context.getFilesDir(), fileName);
        Note note;

        if (file.exists()) {
            FileInputStream fileInputStream;
            ObjectInputStream objectInputStream;

            try {
                // Open FILE
                fileInputStream = context.openFileInput(fileName);
                objectInputStream = new ObjectInputStream(fileInputStream);

                // Read FILE
                note = (Note) objectInputStream.readObject();

                // Close FILE
                fileInputStream.close();
                objectInputStream.close();

            } catch (IOException | ClassNotFoundException e) {
                e.printStackTrace();
                return null;
            }
            return note;
        }
        return null;
    }

    public static void deleteNote(Context context, String fileName) {
        File fileDir = context.getFilesDir();
        File file = new File(fileDir, fileName);

        if (file.exists()) {
            file.delete();
        }
    }
}
