package com.example.chimkontran.notetakerapp;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import java.io.Console;

public class NoteActivity extends AppCompatActivity {

    // Variables (Et = Edit text)
    private EditText nEtTitle;
    private EditText nEtContent;

    private String nFileName;
    private Note nLoadedNote;

    private boolean emptyInput = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note);

        nEtTitle = (EditText) findViewById(R.id.note_et_title);
        nEtContent = (EditText) findViewById(R.id.note_et_content);

        nFileName = getIntent().getStringExtra("NOTE_FILE");
        if (nFileName != null && !nFileName.isEmpty()) {
            nLoadedNote = Utilities.getNoteByName(this, nFileName);

            if (nLoadedNote != null) {
                nEtTitle.setText(nLoadedNote.getnTitle());
                nEtContent.setText(nLoadedNote.getnContent());
            }
        }
    }

    // Create button icons from MENU_NOTE_NEW.XML
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_note_new, menu);
        return true;
    }

    // Selected buttons Functions
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Create Save button
            case R.id.action_note_save:
                checkEmpty();
                //saveNote();
                break;

            // Create Delete button
            case R.id.action_note_delete:
                deleteNote();
                break;
        }

        return true;
    }

    // Check empty Input function
    private void checkEmpty(){
        if (nEtTitle.getText().toString().isEmpty()) {
//            System.out.println("Please fill in Title");
            Toast.makeText(this, "Please fill in Title", Toast.LENGTH_SHORT).show();
        } else if (nEtContent.getText().toString().isEmpty()) {
//            System.out.println("Please fill in Content");
            Toast.makeText(this, "Please fill in Content", Toast.LENGTH_SHORT).show();
        } else {
            System.out.println(nEtTitle.getText().toString());
            System.out.println(nEtContent.getText().toString());
            saveNote();
        }
    }

    // Save Note function
    private void saveNote(){
        Note note;
        if (nLoadedNote == null) {
            note = new Note(System.currentTimeMillis(),
                    nEtTitle.getText().toString(),
                    nEtContent.getText().toString());
        } else {
            note = new Note(nLoadedNote.getnDateTime(),
                    nEtTitle.getText().toString(),
                    nEtContent.getText().toString());
        }

        // Toast to check for success/fail
        if (Utilities.saveNote(this, note)) {
            Toast.makeText(this, "Your note is saved!", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            Toast.makeText(this, "Your note is not saved!", Toast.LENGTH_SHORT).show();
        }
        finish();
    }

    // Delete Note function
    private void deleteNote() {
        if (nLoadedNote == null) {
            finish();
        } else {
            // Add alert to confirm user action
            AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                    .setTitle("delete")
                    .setMessage("You want to delete " + nEtTitle.getText().toString() + ". Are you sure? ")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Utilities.deleteNote(getApplicationContext(),
                                    nLoadedNote.getnDateTime() + Utilities.File_Extension);
                            Toast.makeText(getApplicationContext(),
                                    nEtTitle.getText().toString() + " is deleted", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    })
                    .setNegativeButton("No", null)
                    .setCancelable(false);
            dialog.show();

        }
    }
}
